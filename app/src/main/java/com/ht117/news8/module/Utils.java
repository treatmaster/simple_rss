package com.ht117.news8.module;

import com.ht117.news8.model.Channel;
import com.ht117.news8.model.Item;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pqhy on 1/9/2017.
 */

public class Utils {

    public static Channel parseItem(Element node) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Channel object = new Channel();

        for (Field field : Channel.class.getDeclaredFields()) {
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }

            if (node.getElementsByTag(field.getName()).first() != null) {
                if (field.getType().isAssignableFrom(List.class)) {
                    field.set(object, parseList(node.getElementsByTag(field.getName())));
                } else {
                    field.set(object, node.getElementsByTag(field.getName()).first().text());
                }
            }
        }
        return object;
    }

    private static List<Item> parseList(Elements node) throws IllegalAccessException {
        List<Item> items = new ArrayList<>();
        for (Element element : node) {
            Item object = new Item();
            for (Field field : Item.class.getDeclaredFields()) {
                if (!field.isAccessible()) {
                    field.setAccessible(true);
                }

                Element data = null;
                if (field.getName().equalsIgnoreCase("media")) {
                    data = element.getElementsByTag("media:content").first();
                    if (data != null) {
                        field.set(object, data.attr("url"));
                    }
                } else {
                    data = element.getElementsByTag(field.getName()).first();
                    if (data != null) {
                        field.set(object, data.text());
                    }
                }
            }
            items.add(object);
        }
        return items;
    }
}
