package com.ht117.news8.module;

import android.os.AsyncTask;
import android.util.Log;

import com.ht117.news8.model.Channel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pqhy on 1/10/2017.
 */

public class ChannelLoader extends AsyncTask<String, Void, List> {

    private static List<ChannelListener> listeners = new ArrayList<>();

    public static void subscribe(ChannelListener listener) {
         if (!listeners.contains(listener)) {
             listeners.add(listener);
         }
    }

    public static void unsubscribe(ChannelListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    @Override
    protected List doInBackground(String... urls) {
        List<Channel> channels = new ArrayList<>();
        try {
            for (String url : urls) {
                Document document = Jsoup.connect(url).get();
                channels.add(Utils.parseItem(document.getElementsByTag(Channel.class.getSimpleName().toLowerCase()).first()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for (String url : urls) {
            Log.d("TheShitCode", url);
        }
        return channels;
    }

    @Override
    protected void onPostExecute(List list) {
        super.onPostExecute(list);
        for (ChannelListener listener : listeners) {
            listener.loadDone(list);
        }
    }

    public interface ChannelListener {
        void loadDone(List channels);
    }
}
