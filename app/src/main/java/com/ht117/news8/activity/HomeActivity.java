package com.ht117.news8.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ht117.news8.R;
import com.ht117.news8.adapter.NewsAdapter;

public class HomeActivity extends AppCompatActivity implements NewsAdapter.NewsListener{

    private RecyclerView news;
    private NewsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        NewsAdapter.subscribe(this);
        news = (RecyclerView) findViewById(R.id.news);
        news.setLayoutManager(new LinearLayoutManager(this));
        adapter = new NewsAdapter(this);
        news.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NewsAdapter.unsubscribe(this);
    }

    @Override
    public void loadMore() {
        Toast.makeText(this, "Loading more...", Toast.LENGTH_SHORT).show();
    }
}
