package com.ht117.news8.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ht117.news8.R;
import com.ht117.news8.model.Channel;
import com.ht117.news8.model.Item;
import com.ht117.news8.module.ChannelLoader;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by pqhy on 1/12/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> implements ChannelLoader.ChannelListener {

    private Context context;
    private String[] channels;
    private int index;
    private List<Item> items;
    private static List<NewsListener> listeners = new ArrayList<>();

    public NewsAdapter(Context context) {
        this.context = context;
        ChannelLoader.subscribe(this);
        index = 0;
        channels = context.getResources().getStringArray(R.array.rss);

        items = new ArrayList<>();

        new ChannelLoader().execute(channels[index++]);
    }

    public static void subscribe(NewsListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public static void unsubscribe(NewsListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_news, null);
        NewsHolder holder = new NewsHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        Item item = items.get(position);
        holder.title.setText(item.getTitle());
        holder.desc.setText(item.getDescription());
        Date date = new Date(item.getPubDate());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        holder.pubDate.setText(sdf.format(date));
        if (item.getMedia() != null) {
            Picasso.with(context).load(item.getMedia()).into(holder.thumb);
        }
        if (position == items.size() - 1 && index < channels.length) {
            for (NewsListener listener : listeners) {
                listener.loadMore();
            }
            new ChannelLoader().execute(channels[index++]);
        }
    }

    @Override
    public void loadDone(List data) {
        Log.d("TheShitCode", "Loaded channel " + channels[index-1]);
        items.addAll(((Channel) data.get(0)).getItems());
        notifyDataSetChanged();
    }

    public class NewsHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView desc;
        TextView pubDate;
        ImageView thumb;

        public NewsHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            desc = (TextView) itemView.findViewById(R.id.tvDesc);
            pubDate = (TextView) itemView.findViewById(R.id.tvPubDate);
            thumb = (ImageView) itemView.findViewById(R.id.ivThumb);
        }
    }

    public interface NewsListener {
        void loadMore();
    }
}
